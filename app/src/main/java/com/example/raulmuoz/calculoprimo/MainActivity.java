package com.example.raulmuoz.calculoprimo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    public static String TAG="p01prime:";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Event handler
        Button b = findViewById(R.id.btnCalcular);
        b.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Log.d(TAG,"entrando en OnCreate");
        TextView numero=findViewById(R.id.txtNumero);
        TextView resultado=findViewById(R.id.txtResultado);

        String s=numero.getText().toString();
        int n=Integer.parseInt(s);

        PrimeNumbersCollection c;
        try {
            c = new PrimeNumbersCollection(n);
            resultado.setText(c.lastPrime()+"");
        } catch (Exception e) {
            resultado.setText("Exception occurred");
        }  finally {
        }

        Log.d(TAG,"saliendo de OnCreate");
    }
}
