package com.example.raulmuoz.calculoprimo;

import java.util.ArrayList;

public class PrimeNumbersCollection {

    private ArrayList<Integer> p;

    //constructor, n=number of primes in the list
    public PrimeNumbersCollection(int n) throws Exception {
        if (n<=0)
            throw new Exception("PrimeNumbersCollection: n must be >0");
        p=new ArrayList<>();
        p.add(2);   //add the first prime "2" to the list "p"
        int D=3; //begin our test from 3...
        while (n>p.size()) { //while we haven't got "n" prime numbers
            boolean isPrime=true;  //prime until a exact division is found
            int i=0;
            boolean stay=true;
            do {
                while (i<=p.size()-1 && stay && isPrime) {
                    stay=(p.get(i).intValue()<=Math.sqrt(D));  //don't test divisors > sqrt(n)
                    if (stay) {
                        if (D % p.get(i) == 0)
                            isPrime = false;
                        i++;
                    }
                }
            } while (isPrime && n>p.size() && stay);
            if (isPrime)
                p.add(D);
            D+=2;
        }
    }

    //copy constructor
    public PrimeNumbersCollection(ArrayList<Integer> p) {
        this.p = new ArrayList<>();
        for (Integer i: p) {
            this.p.add(i);
        }
    }

    public Integer lastPrime(){
        return p.get(p.size()-1);
    }

    @Override
    public String toString() {
        String s="";
        for (Integer i: p)
            s+=i+",";
        return s;
    }
}
